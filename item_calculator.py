import sys

from PySide6 import QtWidgets

from app.config import load_config
from app.ui.mainwindow import MainWindow

app = QtWidgets.QApplication([])

load_config("./data.yml")

main_window = MainWindow()

sys.exit(app.exec_())
