import yaml


def get_config():
    global __c

    if not __c:
        __c = {}
    return __c


def load_config(file_path):
    global __c

    with open(file_path, 'r') as stream:
        __c = yaml.safe_load(stream)

    return True


def calc_price(base_price: int, adjustment: float):

    return int(round(base_price * (1 + adjustment), 0))


def get_availability_mod(availability: str):

    availability_dict = {"very rare": 0.3,
                         "rare": 0.2,
                         "scarce": 0.1,
                         "average": 0.0,
                         "common": -0.1,
                         "plentiful": -0.2,
                         "abundant": -0.3}

    return availability_dict[availability]


def beautify_price(price: int):

    tmp = str(price)
    bea_price = ""

    bea_price += f"{tmp[-2:]} CP"
    tmp = tmp[:-2]
    if tmp:
        bea_price = f"{tmp[-2:]} SP " + bea_price
        tmp = tmp[:-2]
    if tmp:
        bea_price = f"{tmp[-2:]} EP " + bea_price
        tmp = tmp[:-2]
    if tmp:
        bea_price = f"{tmp[-2:]} GP " + bea_price
        tmp = tmp[:-2]
    if tmp:
        bea_price = f"{tmp[-2:]} PP " + bea_price

    return bea_price


def write_config(file_path: str):
    with open(file_path, 'w', encoding='utf8') as outfile:
        yaml.dump(__c, outfile, default_flow_style=False, allow_unicode=True)


__c = {}
