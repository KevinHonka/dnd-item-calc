from PySide6 import QtCore
from PySide6.QtCore import Qt
from PySide6.QtGui import QAction
from PySide6.QtWidgets import QMainWindow, QFileDialog, QMessageBox, \
    QDialog, QLabel, QVBoxLayout, QHBoxLayout, QWidget, QPushButton, QLineEdit, QComboBox, QStackedWidget, QTableWidget, \
    QTableWidgetItem, QHeaderView
from qt_material import QtStyleTools

from app.config import load_config, get_config, calc_price, get_availability_mod, beautify_price


class MainWindow(QMainWindow, QtStyleTools):
    BASE_WINDOW_TITLE = "D&D Item Price Calculator"

    def __init__(self):
        super().__init__()

        self.apply_stylesheet(self, theme='light_blue.xml')

        self.create_menu()

        self.create_centralwidget()

        self.init_config()
        self.init_interface()

    def init_config(self):
        self.config_dialog = ConfigDialog()

    def init_interface(self):
        self.setWindowTitle(self.BASE_WINDOW_TITLE)

        self.resize(800, 600)

        self.show()

    def create_centralwidget(self):
        self.central_widget = QStackedWidget()

        # Adding all Widgets to the stack
        self.main_widget = MainWidget()

        self.central_widget.addWidget(self.main_widget)

        self.setCentralWidget(self.central_widget)

    def create_menu(self):
        main_menu = self.menuBar()
        file_menu = main_menu.addMenu("File")
        config_menu = main_menu.addMenu("Configuration")
        help_menu = main_menu.addMenu("Help")

        help_action = QAction("Information", self)
        help_action.triggered.connect(self.show_help)

        config_action = QAction("Change Configuration", self)
        config_action.triggered.connect(self.show_config)

        help_menu.addAction(help_action)
        config_menu.addAction(config_action)

    def show_config(self):
        self.config_dialog.exec_()

    def show_error(self, error):
        error_box = QMessageBox()
        error_box.setWindowTitle("Error")
        error_box.setIcon(QMessageBox.Critical)
        error_box.setText("an error occured")
        error_box.setDetailedText(f"Exception: {error}")

        error_box.exec_()

    def show_help(self):
        information_box = QDialog()
        information_layout = QVBoxLayout()

        information_box.setWindowTitle("Information")

        information_box.setLayout(information_layout)
        information_label = QLabel("If you need help, please visit:")
        url_label = QLabel("<a href=\"https://gitlab.com/KevinHonka/aurora_tools\">Gitlab Repository</a>")
        url_label.setTextFormat(Qt.RichText)
        url_label.setTextInteractionFlags(Qt.TextBrowserInteraction)
        url_label.setOpenExternalLinks(True)
        information_label.setTextInteractionFlags(Qt.TextSelectableByMouse)

        information_layout.addWidget(information_label)
        information_layout.addWidget(url_label)

        information_box.exec_()


class ConfigDialog(QDialog, QtStyleTools):

    def __init__(self):
        super().__init__()

        self.apply_stylesheet(self, theme='light_blue.xml')

        self.setWindowTitle("Configuration")
        self.setWindowModality(Qt.ApplicationModal)

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.setWindowFlag(Qt.WindowCloseButtonHint, False)

        self.init_ui()
        self.init_dialog()

    def init_dialog(self):
        self.dlg = QFileDialog(filter="Yaml Files (*.yml)")
        self.dlg.setFileMode(QFileDialog.AnyFile)

    def init_ui(self):
        config_label_layout = QVBoxLayout()
        config_field_layout = QVBoxLayout()
        config_layout = QHBoxLayout()

        config_label_widget = QWidget()
        config_field_widget = QWidget()
        config_widget = QWidget()

        config_widget.setLayout(config_layout)

        config_label_widget.setLayout(config_label_layout)
        config_field_widget.setLayout(config_field_layout)

        self.confirm_button = QPushButton("Confirm")
        self.confirm_button.setDisabled(True)
        cancel_button = QPushButton("Cancel")
        button_layout = QHBoxLayout()
        button_widget = QWidget()

        self.confirm_button.clicked.connect(self.update_config)
        cancel_button.clicked.connect(self.close)

        button_layout.addWidget(self.confirm_button)
        button_layout.addWidget(cancel_button)
        button_layout.setContentsMargins(5, 5, 5, 5)
        button_widget.setLayout(button_layout)

        db_layout = QHBoxLayout()
        self.config_textfield = QLineEdit()
        db_button = QPushButton("...")
        db_button.clicked.connect(self.update_config_field)
        db_widget = QWidget()
        db_widget.setLayout(db_layout)

        db_layout.addWidget(self.config_textfield)
        db_layout.addWidget(db_button)

        config_layout.addWidget(config_label_widget)
        config_layout.addWidget(config_field_widget)

        self.layout.addWidget(db_widget)
        self.layout.addWidget(config_widget)
        self.layout.addWidget(button_widget)

    def update_config(self):

        load_config(self.config_textfield.text())
        self.close()

    def update_confirm_button(self):

        if not self.config_textfield.text():
            self.confirm_button.setDisabled(True)
        else:
            self.confirm_button.setDisabled(False)

    def update_config_field(self):
        if self.dlg.exec_():
            filenames = self.dlg.selectedFiles()
            self.config_textfield.setText(filenames[0])

            self.update_confirm_button()


class MainWidget(QWidget):
    table_headers = ["Item", "Base Price", "Current Price", "Availability", "Status"]
    table = None

    def __init__(self):
        super().__init__()

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setAlignment(Qt.AlignTop)
        self.setLayout(self.layout)

        self.config = get_config()

        self.init_searchbar()
        self.init_table()
        self.show()

    def init_searchbar(self):
        search_widget = QWidget()
        search_layout = QHBoxLayout()

        self.search_box = QComboBox()
        self.search_box.currentIndexChanged.connect(self.combo_update)

        if len(self.config.keys()) > 0:
            for key, value in self.config["cities"].items():
                self.search_box.addItem(key)

        search_layout.setContentsMargins(5, 5, 5, 5)
        search_layout.addWidget(self.search_box)

        search_widget.setLayout(search_layout)
        self.layout.addWidget(search_widget)

    def init_table(self):
        self.table = QTableWidget()
        self.table.setObjectName("data_table")
        self.table.setColumnCount(4)
        self.table.setRowCount(0)

        hheader = QHeaderView(QtCore.Qt.Orientation.Horizontal)
        self.table.setHorizontalHeader(hheader)
        self.table.setHorizontalHeaderLabels(self.table_headers)

        header = self.table.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setSectionResizeMode(1, QHeaderView.Stretch)
        header.setSectionResizeMode(2, QHeaderView.Stretch)
        header.setSectionResizeMode(3, QHeaderView.Stretch)

        self.layout.addWidget(self.table)
        self.populate_table()

    def combo_update(self, ix):

        if self.table is not None:
            self.populate_table()

    def populate_table(self):

        city = self.search_box.currentText()

        if len(self.config['cities']) > 0:
            row = 0
            for key, value in self.config['cities'][city]['wares'].items():
                item = QTableWidgetItem(key)
                item.setTextAlignment(Qt.AlignCenter)
                self.table.setRowCount(len(self.config['cities'][city]['wares'].keys()))
                self.table.setItem(row, 0, item)

                for x, y in value.items():
                    if x == 'base_price':
                        item = QTableWidgetItem(beautify_price(y))
                        item.setTextAlignment(Qt.AlignCenter)
                        self.table.setItem(row, 1, item)

                    if x == 'availability':
                        availability = get_availability_mod(y)
                        item.setTextAlignment(Qt.AlignCenter)
                        curent_price = calc_price(value['base_price'], availability)
                        item = QTableWidgetItem(beautify_price(curent_price))
                        item.setTextAlignment(Qt.AlignCenter)
                        self.table.setItem(row, 2, item)

                        item = QTableWidgetItem(str(y + f": {availability}"))
                        item.setTextAlignment(Qt.AlignCenter)
                        self.table.setItem(row, 3, item)

                    if x == 'status':
                        item = QTableWidgetItem(y)
                        item.setTextAlignment(Qt.AlignCenter)
                        self.table.setItem(row, 4, item)

                row += 1

        self.update()
